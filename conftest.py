import pytest
from selenium import webdriver
from test_options import IrlixPubOptions as OPTION


@pytest.fixture(scope="session")
def browser():
    driver = webdriver.Chrome(options=OPTION.chrome_options)
    yield driver
    driver.quit()
