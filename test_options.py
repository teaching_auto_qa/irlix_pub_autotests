from time import sleep
from random import randrange
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By


class IrlixPubOptionsLocators:
    IRLIXPUB_PAGE_LOADER = (By.CLASS_NAME, "seq-preloader")
    IRLIXPUB_SEARCH_FIELD = (By.CSS_SELECTOR, ".search .search__input")
    IRLIXPUB_SEARCH_BUTTON = (By.CSS_SELECTOR, ".search .search__button")
    IRLIXPUB_TARGET_CARD = (By.XPATH, f'//a[@href="/drinks/6"]/*/img[@class="drinks-item__image"]')
    IRLIXPUB_EXCERPT_TEXT = (By.CLASS_NAME, "excerpt")
    IRLIXPUB_LESSON3_CARD_BADGE = (By.XPATH, "//a[@href=\"/drinks/4\"]//*[@class=\"badge__value\"]")
    IRLIXPUB_LESSON3_CONTAINER = (By.CLASS_NAME, "dotted-list__item")
    IRLIXPUB_BADGE_VALUE = (By.CSS_SELECTOR, ".drinks-item .badge .badge__value")
    IRLIXPUB_ELEMENT_CONTAINER = (By.CLASS_NAME, "dotted-list__item")
    TESTCASE1_LESSON4_RAND_CARD = (By.XPATH, f"//a[{randrange(16)}][contains(@href, '/drinks/')]")
    TESTCASE1_LESSON4_CARD_TITLE


class IrlixPubOptions:
    # $Browser
    browser_win_size: str = "800,600"
    # browser settings
    chrome_options: Options = Options()
    chrome_options.add_argument("--window-size=" + browser_win_size)

    # $BaseClass
    target_site: str = "http://109.195.203.123:48883/"
    site_title: str = "React Redux App"
    page_loader: str = "seq-preloader"

    # $PageClass
    time_to_press: float = 0.46

    # $Tests
    # options
    searching_card_name: str = "Garry1"
    searching_card_name_lesson3: str = "Jhonny"
    searching_excerpt_text: str = "nice16, nice17, nice18"
    alcohol_perсent_volume: str = "56%"
    target_banana: str = "banana,30"
    target_cucumber: str = "cucumber,30"
    target_test3: dict = {"percent": "80%",
                          "name": "banana",
                          "value": "50 ml"}


class DebugInfo:
    # Funcs for debugging
    @staticmethod
    def debug(**kwargs):
        print(kwargs)
        if len(kwargs) > 1:
            for key, value in kwargs:
                print(f"\n\nDEBUG INFO {key}: {value}")
        elif len(kwargs) == 1:
            for key, value in kwargs:
                print(f"\n\nDEBUG INFO {key}: {value}")
        else:
            print("And what you want debug print?")

    @staticmethod
    def wait_4_test(time_for_waite):
        return sleep(time_for_waite)