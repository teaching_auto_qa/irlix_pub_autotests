from selenium.webdriver import ActionChains as AC
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from test_options import IrlixPubOptions as OPTION


class BasePage:

    def __init__(self, driver):
        self.driver = driver
        self.base_url: str = OPTION.target_site
        self.page_title: str = OPTION.site_title

    def go_to_site(self, time=10):
        site: object = self.driver.get(self.base_url)
        WebDriverWait(self.driver, time).until(EC.title_is(OPTION.site_title), message=f"Page title {self.driver.title} is not equal title {OPTION.site_title} in test")
        return site

    def check_loader_not_show(self, locator, time=10):
        return WebDriverWait(self.driver, time).until_not(EC.presence_of_element_located(locator), message=f"Problem with loader {locator}")

    def find_element(self, locator, time=10):
        return WebDriverWait(self.driver, time).until(EC.presence_of_element_located(locator), message=f"Can't find element by locator {locator}")

    def find_elements(self, locator, time=10):
        return WebDriverWait(self.driver, time).until(EC.presence_of_all_elements_located(locator), message=f"Can't find elements by locator {locator}")

    def check_ready_for_click(self, locator, time=10):
        return WebDriverWait(self.driver, time).until(EC.element_to_be_clickable(locator), message=f"Can't clickable elements by locator {locator}")

    def go_back_from_card(self):
        self.driver.back()

    def click_mouse_on_middle_element(self, locator):
        AC(self.driver).move_to_element(locator).perform()
