import time
from BaseApp import BasePage as BP
from test_options import IrlixPubOptionsLocators as LOCATOR
from test_options import IrlixPubOptions as OPTION
from test_options import DebugInfo as DI

class SearchHelper(BP):


    def check_page_ready(self):
        return self.check_loader_not_show(LOCATOR.IRLIXPUB_PAGE_LOADER)

    def type_text_in_field(self, locator, text):
        self.check_page_ready()
        search_field = self.find_element(locator, time=2)
        search_field.click()
        word_array = list(text)
        for i_char in word_array:
            search_field.send_keys(i_char)
            # DI.wait_4_test(OPTION.time_to_press) # For debug uncomment this line
        return search_field

    def paste_text_in_field(self, locator, text):
        self.check_page_ready()
        search_field = self.find_element(locator, time=2)
        search_field.click()
        search_field.send_keys(text)
        return search_field

    def click_on_element(self, locator):
        self.check_page_ready()
        self.check_ready_for_click(locator)
        self.find_element(locator, time=2).click()

    def go_card_by_element(self, locator):
        self.check_page_ready()
        self.find_element(locator, time=2).click()

    def take_text_from_element(self, locator):
        self.check_page_ready()
        return self.find_element(locator, time=2).text

    def compare_titles(self, first_locator, second_locator) -> bool:
        result = self.take_text_from_element(first_locator) == self.take_text_from_element(second_locator)
        assert result, f"compare_titles() result: Заголовки отличаются!"
        return result

    def find_parent_by_xpath(self, child_element, selector, child_attribute, selector_attribute):
        self.check_page_ready()
        current_element = child_element
        xpath = f"//*[contains(@{selector}, '{child_attribute}')]//ancestor::*[contains(@{selector}, '{selector_attribute}')]"
        current_element = current_element.find_element(By.XPATH, xpath)
        return current_element

    def parsing_cards_text_in_element(self, locator, card_filter, position: int = 0) -> dict:
        self.check_page_ready()
        found_cards: list = []
        print(type(found_cards))
        for card in self.find_elements(locator, time=10):
            print(type(card))
            if card.text == card_filter:
                found_cards.append(card)
        # print(f"\n found_cards[0] {found_cards[0]}")
        # print(f"\n found_cards[1] {found_cards[1]}")
        # print(f"\n found_cards[2] {found_cards[2]}")
        # print(f"\n found_cards[3] {found_cards[3]}")
        length_found_cards_array = int(len(found_cards))
        # print(f"\n position {position}")
        # print(f"\n found_cards[position] {found_cards[position]}")
        assert length_found_cards_array > 0, f"\n Cards not found"
        return {"current_card": found_cards[position], "length": length_found_cards_array}

    def checking_card_ingredient_list(self, container_locator, searching_text) -> bool:
        self.check_page_ready()
        found_elements = set()
        element_number: int = 0
        param1: bool = False
        param2: bool = False
        searching_text_dict: dict = searching_text
        container_items_array = self.find_elements(container_locator, time=2)
        current_items: list = []

        for container_items in container_items_array:
            print(f"\n container_items@ {container_items}")
            element_number += 1
            current_items.append(container_items.find_element_by_xpath(f'//*[{element_number}][@class="dotted-list__item"]//*[@class="dotted-list__item-name"]'))
            current_items.append(container_items.find_element_by_xpath(f'//*[{element_number}][@class="dotted-list__item"]//*[@class="dotted-list__item-value"]'))
            # for container_item in container_items:
            # if element_number == 0:
            param1 = current_items.pop(0).text == searching_text_dict.get("name")
            # elif element_number == 1:
            param2 = current_items.pop().text == searching_text_dict.get("value")
            if param1 and param2:
                found_elements.add(element_number)
        result = len(found_elements) > 0
        assert result, "Not found element in containers"
        return result

    def search_cards_by_element_text(self, locator, card_filter, container_locator, searching_text):
        position = 0
        length = 0
        current_card = None
        found_cards: list = []
        while position <= length:
            filtered_card_dict = self.parsing_cards_text_in_element(locator, card_filter, position)
            position += 1
            print(f"\n filtered_card_dict {filtered_card_dict}")
            current_card = filtered_card_dict.pop('current_card')
            print(f"\n current_card {current_card}")
            length = filtered_card_dict.pop("length")
            print(f"\n length {length}")
            root_element_card = self.find_parent_by_xpath(current_card, "class", "badge__value", "drinks__link")
            root_element_card.click()
            self.check_page_ready()
            self.go_back_from_card()
            self.check_page_ready()


        # for card in filtered_cards_array:
        #     # card.location_once_scrolled_into_view
        #     print(f"\n Array item: {card}")
        #     print(f"\n Array item tag: {card.tag_name}")
        #     print(f"\n Array item text: {card.text}")
        #     print(type(card))
        #     print(f"card is: {card}")
        #     # print(i_count)
        #     root_element_card = self.find_parent_by_xpath(card, "class", "badge__value", "drinks__link")
        #     # i_count += 1
        #     # print(i_count)
        #     print("root: ")
        #     print(root_element_card)
        #     print(root_element_card.tag_name)
        #     # root_element_card.location_once_scrolled_into_view
        #     # self.wait_4_test(22)
        #     print("root end here: ")
        #     print(root_element_card)
        #     print(card)import
        #     # root_element_card.location_once_scrolled_into_view
        #     card.click()
        #     self.driver.close()
        #     self.go_to_site()
        #     self.check_page_ready()
        #     # self.checking_card_ingredient_list(container_locator, searching_text)
        #     self.go_back_from_card()
        #     print(filtered_cards_array, card)

        #     found_cards.append(self.checking_card_ingredient_list(card, container_locator, searching_text))
        # assert len(found_cards) > 0
        # return print(f"\nFound cards {found_cards}")


