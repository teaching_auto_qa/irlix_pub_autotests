from IRLIX_Pub_Pages import SearchHelper, LOCATOR, OPTION, DI



# def test_irlixpub_search(browser):
#     irlixpub_main_page = SearchHelper(browser)
#     irlixpub_main_page.go_to_site()
#     irlixpub_main_page.click_on_element(LOCATOR.IRLIXPUB_SEARCH_BUTTON)
#     irlixpub_main_page.type_text_in_field(LOCATOR.IRLIXPUB_SEARCH_FIELD,
#                                           OPTION.searching_card_name)
#     irlixpub_main_page.go_card_by_element(LOCATOR.IRLIXPUB_TARGET_CARD)
#     result1 = irlixpub_main_page.take_text_from_element(LOCATOR.IRLIXPUB_EXCERPT_TEXT)
#     assert result1 in OPTION.searching_excerpt_text


def test_for_lesson3(browser):
    irlixpub_lesson3 = SearchHelper(browser)
    irlixpub_lesson3.go_to_site()
    irlixpub_lesson3.parsing_cards_text_in_element(LOCATOR.IRLIXPUB_LESSON3_CARD_BADGE, OPTION.target_test3.get("percent")).get("current_card").click()
    irlixpub_lesson3.checking_card_ingredient_list(LOCATOR.IRLIXPUB_LESSON3_CONTAINER, OPTION.target_test3)


# def hard_level_test_for_lesson3(browser):
#     irlixpub_hard_lesson3 = SearchHelper(browser)
#     irlixpub_hard_lesson3.go_to_site()
#     irlixpub_hard_lesson3.search_cards_by_element_text(LOCATOR.IRLIXPUB_BADGE_VALUE,
#                                                        OPTION.alcohol_perсent_volume,
#                                                        LOCATOR.IRLIXPUB_ELEMENT_CONTAINER,
#                                                        OPTION.target_test3)

def test_case_1_for_lesson4(browser):
    irlixpub_tc_1_l4 = SearchHelper(browser)
    irlixpub_tc_1_l4.go_to_site()
    irlixpub_tc_1_l4.click_on_element(LOCATOR.TESTCASE1_LESSON4_RAND_CARD)
    lesson4_title_card = irlixpub_tc_1_l4.take_text_from_element()
    DI.wait_4_test(4)
    pass

def test_case_2_for_lesson4(browser):
    irlixpub_tc_2_l4 = SearchHelper(browser)
    pass

def test_case_3_for_lesson4(browser):
    irlixpub_tc_3_l4 = SearchHelper(browser)
    pass
